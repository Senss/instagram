import re


class Authorization:
    def __init__(self, login, password, chars):
        self.login = check_login(login)
        self.password = check_password(login, password)

    def login_data(login):
        with open('all_login.txt') as file:
            if re.findall(login, file):
                return login
            else:
                print('Пользователь не найдён')

    def check_password(login, password):
        with open(login + '.txt') as file:
            if re.findall(password, file):
                return password
            else:
                print('Неверно введен пользователь или пароль')


class Register:
    def __init__(self, name, surname, login, email, telephone, password,
                 password_check):
        self.name = name
        self.surname = surname
        self.login = check_login(login)
        self.email = check_email(email)
        self.telephone = telephone
        if password_check == password:
            self.password = password
        else:
            print('Пароли не совпадают')
        get_data(name, surname, login, email, telephone, password)

    def check_login(login):
        # пусть у нас имеется файл куда записываюстя все логины
        with open('all_login.txt', 'r+') as file:
            if re.findall(login, file):
                print('Логин занят')
            else:
                file.write(' ' + login)
                return login

    def check_email(mail):
        if re.findall(r'.*@((mail.ru)|(yandex.ru)|(inbox.ru)|(list.ru)|(bk.ru)'
                      r'|(yandex.ru)|(gmail.ru)|(yahoo.ru)|(hotmail.ru)|(outlook.ru))', mail):
            return mail
        else:
            print('Почта была введена не коректно')

    def get_data(name, surname, login, email, telephone, password):
        with open(login + '.txt', w) as file:
            # helper нужен что бы избежать повторения кода
            helper = [name, surname, email, telephone, password]
            for i in helper:
                file.write(i)


class Scope_of_account:
    def __init__(self, sphere, typec):
        self.sphere = sphere
        self.typec = typec

    def print_info(self):
        print(f"Sphere: {self.sphere}, Types: {self.types}")


class Statistics:
    def __init__(self, number_of_views,
                 number_of_subscribers, number_of_likes, views):
        self.number_of_views = number_of_views
        self.number_of_subscribers = number_of_subscribers
        self.number_of_likes = number_of_publications
        self.views = views


    def growth_in_statistics(self, growth):
        for i in range(100):  # Рост за 99 дней
            number_of_views = number_of_views + growth
            number_of_subscribers = number_of_subscribers + growth
            number_of_likes = number_of_likes + growth
        print(f"Statistics_views_subscribers_likes: {number_of_views, number_of_subscribers, number_of_likes}")

    def account_success(self):
        if self.views > self.number_of_views:
            print("Account: НЕУСПЕШНЫЙ")
        else:
            print("Account: УСПЕШНЫЙ")


class Description:
    def __init__(self, name, age, hobby, personal_qualities):
        self.name = name
        self.age = age
        self.hobby = hobby
        self.personal_qualities = personal_qualities

    def print_description(self):
        print(f"Name: {self.name}, Age: {self.age}, Hobby: {self.hobby}, personal_qualities: {self.personal_qualities}")

